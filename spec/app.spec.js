let taskCompare = require("../app");

describe("Add functionality", () => {

    // Common + Task-1 tests
    it("creates a new instance of Task1Compare and checks if its prototype is Task1Compare",
     () => {
        let instance = new taskCompare(5);
        expect(typeof instance).toMatch("object");
        expect(instance instanceof taskCompare).toBe(true);
    });

    it("checks creating an array if instance property 'n' is typeof 'string'", () => {
        let instance = new taskCompare("dsdsd");
        expect(instance.calculateTask1()).toBe(0);
    });

    it("checks creating an array if instance property 'n' is 0", () => {
        let instance = new taskCompare(0);
        expect(instance.calculateTask1()).toBe(0);
    });

    it("checks creating an array if instance property 'n' is negative integer value", () => {
        let instance = new taskCompare(-5);
        expect(instance.calculateTask1()).toBe(0);
    });

    it("checks if created Array consists of 'n' elements", () => {
        let n = 5;
        let instance = new taskCompare(n);
        instance.calculateTask1();
        expect(instance.array.length).toBe(n);
    });

    it("checks if created array consists of number elements", () => {
        let n = 5;
        let instance = new taskCompare(n);
        instance.calculateTask1();
        let isAllElementsNumbers = (() => {
            for (let i = 0, len = instance.array.length; i < len; i++) {
                if (typeof instance.array[i] !== "number") {
                    return 0;
                }
            }
            return 1;
        })();        
        expect(isAllElementsNumbers).toEqual(1);
    });

    it("checks if function 'calculateTask1' calls function 'fillAray' with n was set\
 in a construnctor", () => {
        let instance = new taskCompare(3);
        spyOn(instance, 'fillArrayWithRandomNumbers').and.returnValue([3, 6, 8]);
        instance.calculateTask1();
        expect(instance.fillArrayWithRandomNumbers).toHaveBeenCalled();
    });

    it("checks if function 'calculateTask1' calls other functions in a right way", () => {
        let instance = new taskCompare(3);
        instance.array = [54, 68, 90];

        spyOn(instance, 'fillArrayWithRandomNumbers');
        spyOn(instance, 'doCalculationsTask1');

        instance.calculateTask1();
        expect(instance.doCalculationsTask1).toHaveBeenCalledTimes(1);
        expect(instance.fillArrayWithRandomNumbers.calls.any()).toBe(false);
    });

    it("checks if function 'calculateTask1' returns 0 when 'n' is not defined with use of\
 constructor and array was defined manually\
 (with no use of 'fillArrayWithRandomNumbers' function)", () => {
        let instance = new taskCompare();
        instance.array = [54, 68, 90];
        instance.calculateTask1();
        expect(instance.calculateTask1()).toBe(0);
    });

    it("checks if function 'calculateTask1' returns 0 when 'n' is not defined with use of\
 constructor and array was defined manually\
 (with no use of 'fillArrayWithRandomNumbers' function)", () => {
        let instance = new taskCompare(3);
        instance.array = ["test", "test", "test"];
        instance.calculateTask1();
        expect(instance.calculateTask1()).toBe(0);
        });

    it("checks if function 'doCalculationsTask1' calculates an expression in proper way", () => {
        let instance = new taskCompare(3);
        instance.array = [10, 73, 44];
        instance.calculateTask1();
        expect(instance.counterTask1).toBe(0);
    });
    
    it("checks if function 'doCalculationsTask1' calculates an expression in proper way", () => {
        let instance = new taskCompare(3);
        instance.array = [54, 68, 90];
        instance.calculateTask1();
        expect(instance.counterTask1).toBe(1);
    });

    // Task-2 tests
    it("checks if function 'calculateTask2' calls other functions in a right way", () => {
        let instance = new taskCompare(3);
        instance.array = [54, 68, 90];

        spyOn(instance, 'fillArrayWithRandomNumbers');
        spyOn(instance, 'doCalculationsTask2');

        instance.calculateTask2();
        expect(instance.doCalculationsTask2).toHaveBeenCalledTimes(1);
        expect(instance.fillArrayWithRandomNumbers.calls.any()).toBe(false);
    });

    it("checks if function 'calculateTask2' calls other functions in a right way", () => {
        let instance = new taskCompare(3);
        instance.array = [54, 68, 90];
        spyOn(instance, 'factorial');
        instance.calculateTask2();
        expect(instance.factorial).toHaveBeenCalledTimes(3);
    });

    it("checks if function 'factorial' calculates factorial in proper way", () => {
        let instance = new taskCompare();
        expect(instance.factorial(-5)).toBe(0);
        expect(instance.factorial(0)).toBe(1);
        expect(instance.factorial(2)).toBe(2);
        expect(instance.factorial(4)).toBe(24);
    });

    it("checks if function 'doCalculationsTask2' calculates an expression in proper way", () => {
        let instance = new taskCompare(5);
        instance.array = [14, 16, 74, 17, 20];
        instance.calculateTask2();
        expect(instance.counterTask2).toBe(1);
    });

    it("checks if function 'doCalculationsTask2' calculates an expression in proper way", () => {
        let instance = new taskCompare(4);
        instance.array = [77, 12, 56, 92]
        instance.calculateTask2();
        expect(instance.counterTask2).toBe(0);
    });
});
