
class PascalTriangle {
  constructor(levels) {
    this.levels = levels;
  }

  // Task-3
  build() {
    this.buildLevel(1);
  }

  buildLevel(level, prevRow) {
    const numbers = this.calculateRowNumbers(level, prevRow);
    this.showRow(numbers);

    if (level < this.levels) {
      return this.buildLevel(level + 1, numbers);
    }
    return 0;
  }

  calculateRowNumbers(level, prevRow) {
    const numbers = [];

    if (level === 1) {
      numbers.push(1);
      return numbers;
    }

    for (let i = 0; i < level; i += 1) {
      if ((i === 0) || (i === level - 1)) {
        numbers.push(1);
      } else {
        numbers.push(prevRow[i - 1] + prevRow[i]);
      }
    }

    return numbers;
  }

  showRow(numbers) {
    console.log(...numbers);
  }
}

const triangle = new PascalTriangle(process.argv[2]);
triangle.build();
