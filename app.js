class TasksCompare {
  constructor(n) {
    this.n = n;
    this.array = [];
    this.counterTask1 = 0;
    this.counterTask2 = 0;
  }

  fillArrayWithRandomNumbers() {
    const randomNumbersArray = [];

    for (let i = 0; i < this.n; i += 1) {
      randomNumbersArray[i] = Math.floor((Math.random() * 100) + 1);
    }
    return randomNumbersArray;
  }

  checkInputArray() {
    for (let i = 0, len = this.array.length; i < len; i += 1) {
      if (typeof this.array[i] !== 'number') {
        return 0;
      }
    }
    return 1;
  }

  // Task-1 Part
  calculateTask1() {
    if (this.n >= 3 && this.array.length === 0) {
      this.array = this.fillArrayWithRandomNumbers();
      this.doCalculationsTask1();
      return 1;
    } else if (this.n >= 3 && this.n === this.array.length && this.checkInputArray()) {
      this.doCalculationsTask1();
      return 1;
    }
    console.log('Wrong input data');
    return 0;
  }

  doCalculationsTask1() {
    for (let k = 1; k < this.n - 1; k += 1) {
      if (this.array[k] < ((this.array[k - 1] + this.array[k + 1]) / 2)) {
        this.counterTask1 += 1;
      }
    }
    console.log(`Result of task 1: ${this.counterTask1}`);
  }

  // Task-2 Part
  calculateTask2() {
    if (this.n >= 0 && this.array.length === 0) {
      this.array = this.fillArrayWithRandomNumbers();
      this.doCalculationsTask2();
      return 1;
    } else if (this.n >= 0 && this.n === this.array.length && this.checkInputArray()) {
      this.doCalculationsTask2();
      return 1;
    }
    console.log('Wrong input data');
    return 0;
  }

  doCalculationsTask2() {
    for (let i = 0; i < this.n; i += 1) {
      if ((Math.pow(2, i) < this.array[i]) && (this.array[i]) < this.factorial(i)) {
        this.counterTask2 += 1;
      }
    }
    console.log(`Result of task 2: ${this.counterTask2}`);
  }

  factorial(k) {
    if (k < 0) {
      console.log('Wrong data');
      return 0;
    }
    if (k === 0) {
      return 1;
    }

    return k * this.factorial(k - 1);
  }
}

const test1 = new TasksCompare(Number(process.argv[2]));
test1.calculateTask1();
test1.calculateTask2();
// console.log(test1);

// const test2 = new TasksCompare(10);
// test2.fillArrayWithRandomNumbers(10);
// test2.calculateTask1();
// test2.calculateTask2();

// const test3 = new TasksCompare(3);
// test3.array = [54, 68, 90];
// test3.calculateTask1();
// test3.calculateTask2();

module.exports = TasksCompare;
